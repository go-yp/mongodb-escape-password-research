up:
	sudo docker-compose up -d

down:
	sudo docker-compose down --volumes

mongodb-shell:
	sudo docker exec -it mongodb_escape_password_research bash

mongodb-root-ping:
	sudo docker exec -it mongodb_escape_password_research \
		mongo -u root -p root_password --quiet --eval \
			'db.runCommand("ping").ok' \
				admin

mongodb-root:
	sudo docker exec -it mongodb_escape_password_research \
		mongo -u root -p root_password

mongodb-create-user-tester1:
	sudo docker exec -it mongodb_escape_password_research \
		mongo -u root -p root_password --eval \
			'db.getSiblingDB("tester_db").createUser({"user":"tester1", "pwd":"example+password", "roles": [{"role": "readWrite", "db": "tester_db"}]})'

mongodb-ping-tester1:
	sudo docker exec -it mongodb_escape_password_research \
		mongo -u tester1 -p example+password --quiet --eval \
			'db.runCommand("ping").ok' \
				tester_db


mongodb-create-user-testerN:
	sudo docker exec -it mongodb_escape_password_research \
		mongo -u root -p root_password --eval \
			'db.getSiblingDB("tester_db").createUser({"user":"testerN1", "pwd":"example''password", "roles": [{"role": "readWrite", "db": "tester_db"}]})'

create: mongodb-create-user-tester1
ping: mongodb-ping-tester1
