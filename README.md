### MongoDB escape password research

##### Users
* [How to enable authentication on MongoDB through Docker?](https://stackoverflow.com/q/34559557/17655004)
* [How to create a DB for MongoDB container on start up?](https://stackoverflow.com/q/42912755/17655004)
* [Simple HTTP/TCP health check for MongoDB](https://stackoverflow.com/q/37839365/17655004)
* [How do I create a new MongoDB from the command line with auth turned on?](https://stackoverflow.com/a/11288002/17655004)
* [Escaping Characters in Bash](https://www.baeldung.com/linux/bash-escape-characters)

##### Connect & escape
* [How to connect to a MongoDB in Go?](https://stackoverflow.com/a/60371447/17655004)
* [encodeURIComponent](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/encodeURIComponent)

##### Docker
* [Executing docker command using golang exec fails](https://stackoverflow.com/q/26411594/17655004)

##### Development
```bash
make up

make mongodb-root-ping

make mongodb-create-user-tester1 # success
make mongodb-create-user-tester2 # success

make mongodb-tester1-ping # success
make mongodb-tester2-ping # success

go run main.go # show problem

make down
```

### Result
| SOLUTION | SUCCESS | TOTAL | COMMENT                                                     |
|----------|---------|-------|-------------------------------------------------------------|
| 0        | 36      | 38    | ```url.UserPassword(username, password)```                  |
| 1        | 10      | 38    | ```url.UserPassword(username, url.QueryEscape(password))``` |
| 2        | 35      | 38    | ```"__plus__"```                                            |
| 3        | 36      | 38    | ```"__password__"```                                        |
| 4        | 37      | 38    | ```":__password__@"```                                      |
| 5        | 1       | 38    |                                                             |
