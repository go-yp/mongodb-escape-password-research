package main

import (
	"context"
	"encoding/json"
	"fmt"
	"net/url"
	"os"
	"os/exec"
	"strconv"
	"strings"

	"github.com/pkg/errors"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

const (
	solutionExists = 0
	solutionArtem  = 1 // https://github.com/percona/pmm-managed/pull/1098#discussion_r861838935
	solutionPR     = 2 // https://github.com/percona/pmm-managed/pull/1003/files
	solutionMy1    = 3
	solutionMy2    = 4
	solutionMy3    = 5
	solutionAlexey = 6
)

func main() {
	// https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/encodeURIComponent

	// var set1 = ";,/?:@&=+$";  // Reserved Characters
	// var set2 = "-_.!~*'()";   // Unescaped Characters
	// var set3 = "#";           // Number Sign
	// var set4 = "ABC abc 123"; // Alphanumeric Characters + Space

	var userPasswords = [][3]string{
		{"root", "root_password", "/admin"},
		{"__plus__", "root_password", "/tester_db"},
		{"testerPlus", "__plus__", "/tester_db"},
		{"__password__", "root_password", "/tester_db"},
		{"testerPassword", "__password__", "/tester_db"},
	}

	var symbols = "" +
		";,/?:@&=+$" +
		"-_.!~*'()" +
		"#" +
		" " +
		"%" +
		"[]" +
		"абвгґдеє" +
		"\""

	// on "go run main.go" FAILED ONLY "'"

	for i, symbol := range symbols {
		password := fmt.Sprintf("example%spassword", string(symbol))

		// password = strings.Replace(password, "'", "''", -1)

		userPasswords = append(userPasswords, [3]string{
			"tester" + strconv.Itoa(i),
			password,
			"/tester_db",
		})
	}

	// rewrite to fix pmm-managed tests
	userPasswords = append(userPasswords, [][3]string{
		{"pmm-managed-test-1-line-216", "s3cur3 p@$$w0r4.", "/tester_db"},
	}...)

	// fmt.Println(userPasswords)

	if len(os.Args) == 2 && os.Args[1] == "create" {
		for i, userPassword := range userPasswords {
			// skip root user
			if userPassword[0] == "root" {
				continue
			}

			fmt.Printf("Start command %d\n", i)

			var jsonPassword, marshalErr = json.Marshal(userPassword[1])
			if marshalErr != nil {
				panic(marshalErr)
			}

			// language=Bash
			var (
				arg = fmt.Sprintf(
					`sudo docker exec mongodb_escape_password_research mongo -u root -p root_password --eval 'db.getSiblingDB("tester_db").createUser({"user":"%s", "pwd":%s, "roles": [{"role": "readWrite", "db": "tester_db"}]})'`,
					userPassword[0], jsonPassword,
				)
			)

			var output, err = exec.Command("/bin/sh", "-c", arg).Output()

			if false {
				fmt.Printf("OUTPUT: %q\n, ERROR: %+v\n, PASSWORD: %+v\n", output, err, userPassword[1])
			}

			if err != nil {
				fmt.Printf("\033[0;31m[FAILURE]\033[0m [%2d] password [%18s], err: %+v\n", i, userPassword[1], err)
			} else {
				fmt.Printf("\033[0;32m[SUCCESS]\033[0m [%2d] password [%18s]\n", i, userPassword[1])
			}

			fmt.Printf("End command %d\n", i)
		}

		return
	}

	if len(os.Args) == 2 && os.Args[1] == "ping" {
		var (
			total   = 0
			success = 0
		)

		for i, userPassword := range userPasswords {
			// skip root user
			if userPassword[0] == "root" {
				continue
			}

			total += 1

			fmt.Printf("Start command %2d\n", i)

			var jsonPassword, marshalErr = json.Marshal(userPassword[1])
			if marshalErr != nil {
				panic(marshalErr)
			}

			// language=Bash
			var (
				arg = fmt.Sprintf(
					`sudo docker exec mongodb_escape_password_research mongo -u %s -p %s --quiet --eval 'db.runCommand("ping").ok' tester_db`,
					userPassword[0], jsonPassword,
				)
			)

			var output, err = exec.Command("/bin/sh", "-c", arg).Output()

			if false {
				fmt.Printf("OUTPUT: %q\n, ERROR: %+v\n, PASSWORD: %+v\n", output, err, userPassword[1])
			}

			if err != nil {
				fmt.Printf("\033[0;31m[FAILURE]\033[0m [%2d] password [%18s], err: %+v\n", i, userPassword[1], err)
			} else {
				success += 1

				fmt.Printf("\033[0;32m[SUCCESS]\033[0m [%2d] password [%18s]\n", i, userPassword[1])
			}

			fmt.Printf("End command   %2d\n", i)
		}

		fmt.Printf("%2d / %2d\n", success, total)

		return
	}

	var solutionMap = []int{
		solutionExists: 0,
		solutionArtem:  0,
		solutionPR:     0,
		solutionMy1:    0,
		solutionMy2:    0,
		solutionMy3:    0,
		solutionAlexey: 0,
	}

	for _, userPassword := range userPasswords {
		for _, solution := range []int{
			solutionExists,
			solutionArtem,
			solutionPR,
			solutionMy1,
			solutionMy2,
			solutionMy3,
			solutionAlexey,
		} {
			err := ping(userPassword[0], userPassword[1], userPassword[2], solution)

			if err != nil {
				fmt.Printf("\033[0;31m[FAILURE]\033[0m [%2d] DB %12q, user: %10q, password %18q, err: %+v\n", solution, userPassword[2], userPassword[0], userPassword[1], err)
			} else {
				fmt.Printf("\033[0;32m[SUCCESS]\033[0m [%2d] DB %12q, user: %10q, password %18q\n", solution, userPassword[2], userPassword[0], userPassword[1])

				solutionMap[solution] += 1
			}
		}
	}

	for solution, successCount := range solutionMap {
		if successCount == len(userPasswords) {
			fmt.Printf("\033[0;32m[SUCCESS]\033[0m [SOLUTION = %2d] SUCCESS = %2d / TOTAL = %2d\n", solution, successCount, len(userPasswords))
		} else {
			fmt.Printf("\033[0;31m[FAILURE]\033[0m [SOLUTION = %2d] SUCCESS = %2d / TOTAL = %2d\n", solution, successCount, len(userPasswords))
		}
	}
}

func ping(username, password, db string, solution int) error {
	u := &url.URL{
		Scheme:   "mongodb",
		Host:     "localhost:27017",
		Path:     db,
		RawQuery: "connectTimeoutMS=2000&serverSelectionTimeoutMS=2000",
	}

	var (
		opts *options.ClientOptions
	)

	switch solution {
	// SOLUTION 0
	// SOLUTION NOW EXISTS IN pmm-manager
	// 36 / 38
	case solutionExists:
		u.User = url.UserPassword(username, password)

		var dsn = u.String()

		opts = options.Client().ApplyURI(dsn)
	// SOLUTION 1
	// Artem's propose solution https://github.com/percona/pmm-managed/pull/1098#discussion_r861838935
	// 10 / 38
	case solutionArtem:
		u.User = url.UserPassword(username, url.QueryEscape(password))

		var dsn = u.String()

		opts = options.Client().ApplyURI(dsn)
	// SOLUTION 2
	// Another solution https://github.com/percona/pmm-managed/pull/1003/files
	// 35 / 38
	case solutionPR:
		const plusPlaceholder = "__plus__" // There is a risk of error, when the password contains this string. Can change it to a more unique string.

		password = strings.ReplaceAll(password, "+", plusPlaceholder)

		u.User = url.UserPassword(username, password)

		var dsn = u.String()

		dsn = strings.ReplaceAll(dsn, url.QueryEscape(plusPlaceholder), "%2B")

		opts = options.Client().ApplyURI(dsn)
	// SOLUTION 3
	// 36 / 38
	case solutionMy1:
		u.User = url.UserPassword(username, "__password__")

		var dsn = u.String()

		dsn = strings.Replace(dsn, "__password__", url.QueryEscape(password), 1)

		opts = options.Client().ApplyURI(dsn)
	// SOLUTION 4
	// 37 / 38
	case solutionMy2:
		u.User = url.UserPassword(username, "__password__")

		var dsn = u.String()

		dsn = strings.Replace(dsn, ":__password__@", ":"+url.QueryEscape(password)+"@", 1)

		opts = options.Client().ApplyURI(dsn)
	// SOLUTION 5
	// in this case we NOT HAVE user:pass in DSN
	//  1 / 38
	case solutionMy3:
		var dsn = u.String()

		opts = options.Client().ApplyURI(dsn)
		opts = opts.SetAuth(options.Credential{
			Username: username,
			Password: password,
		})

	// SOLUTION 6
	// in this case we HAVE user:pass in DSN
	case solutionAlexey:
		u.User = url.UserPassword(username, password)

		var dsn = u.String()

		var clientForDSNErr error
		opts, clientForDSNErr = ClientForDSN(dsn)
		if clientForDSNErr != nil {
			return clientForDSNErr
		}
	}

	ctx := context.Background()

	client, err := mongo.Connect(ctx, opts)
	if err != nil {
		return err
	}
	defer client.Disconnect(ctx) //nolint:errcheck

	if err = client.Ping(ctx, nil); err != nil {
		return err
	}

	return nil
}

func ClientForDSN(dsn string) (*options.ClientOptions, error) {
	parsedDsn, err := url.Parse(dsn)
	if err != nil {
		return nil, errors.Wrap(err, "cannot parse DSN")
	}

	clientOptions := options.Client().ApplyURI(dsn)

	// Workaround for PMM-9320
	username := parsedDsn.User.Username()
	password, _ := parsedDsn.User.Password()
	if username != "" || password != "" {
		clientOptions = clientOptions.SetAuth(options.Credential{Username: username, Password: password})
	}

	return clientOptions, nil
}
